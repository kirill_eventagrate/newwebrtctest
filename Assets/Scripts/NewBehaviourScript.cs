﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WebrtcSharp;

public class NewBehaviourScript : MonoBehaviour
{
    PeerConnection Peer;
    PeerConnectionFactory connectionFactory;
    RTCConfiguration configuration;
    public Signaller signaller;
    public int RemoteId;
    public Camera cam;
    public MediaStreamTrack streamTrack;

    public Button SpeakerBtn;
    public Button VisitorBtn;
    // Start is called before the first frame update
    void Start()
    {
        
        SpeakerBtn.onClick.AddListener(() => CreatePeerOffer(ConnectionType.speaker));
        VisitorBtn.onClick.AddListener(() => CreatePeerOffer(ConnectionType.visitor));
        configuration = new RTCConfiguration{
            
        };
        configuration.AddServer("stun:stun.l.google.com:19302");
        connectionFactory = new PeerConnectionFactory();
        Peer = connectionFactory.CreatePeerConnection(configuration);
        Peer.TrackAdded += Peer_TrackAdded;
        //Invoke("CreateSDP", 3f);
    }

    private void Peer_TrackAdded(RtpReceiver obj)
    {
        //throw new System.NotImplementedException();
        //MediaStreamTrack mediaStreamTrack = obj.Track;
        var videoSource = new FrameVideoSource();
        var videoTrack = connectionFactory.CreateVideoTrack("video_label", videoSource);
        videoSource.Frame += VideoSource_Frame;
        Debug.Log("got track");
        
    }

    private void VideoSource_Frame(VideoFrame obj)
    {
        Debug.Log("got frame: " + obj.Height);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnAddIceCandidate(IceCandidate candidate)
    {
        Debug.Log("send candidate: " + candidate.Sdp);
        signaller.SendIceCandidate(candidate.Sdp, candidate.SdpMLineIndex, candidate.SdpMid);
    }

    public void OnIceCandidate(string _candidate, string _sdpMlineIndex, string _sdpMid)
    {
        IceCandidate candidate = new IceCandidate(_sdpMid, int.Parse(_sdpMlineIndex), _candidate);
        Peer.AddIceCandidate(candidate);
        Debug.Log($"Peer ICE candidate:\n {candidate.Sdp}");
    }

    public void SetRemoteDescription(string _data)
    {
        Peer.SetRemoteDescription("answer", _data);
        Debug.Log("get remote sdp: " + _data);
    }

    

    async void CreatePeerOffer(ConnectionType _connectionType)
    {
        string op = await Peer.CreateOffer();
        //yield return op;
        StartCoroutine(OnCreateOfferSuccess(op, _connectionType));
        Debug.Log(op);
    }

    IEnumerator OnCreateOfferSuccess(string desc, ConnectionType _connectionType)
    {
        yield return Peer.SetLocalDescription("offer", desc);
        string CTstr = _connectionType == ConnectionType.speaker ? "speaker" : "client";
        signaller.SendSdpDataToServer(new Message
        {
            ConnectionType = CTstr,
            speakerId = RemoteId,
            Data = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(desc))
        });
    }

    async void CreateSDP()
    {
        var offer = await Peer.CreateOffer();
        Debug.Log(offer);
    }

    private void OnDestroy()
    {
        Peer.Close();
    }
}

[Serializable]
public enum ConnectionType
{
    speaker,
    visitor
}