﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Signaller : MonoBehaviour
{
    public NewBehaviourScript peerUnity;
    public string HttpServerAddress;
    public string LocalPeerId;
    //public string RemotePeerId;
    public bool StartGetIceCandidate = false;

    public float PollTimeMs = 500f;
    public float timeSincePollMs = 0f;
    bool lastComplite = false;
    public int gotError = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timeSincePollMs <= PollTimeMs)
        {
            timeSincePollMs += Time.deltaTime * 1000.0f;
            return;
        }

        if (!lastComplite)
            return;

        timeSincePollMs = 0f;
        lastComplite = false;

        if (gotError < 3 && StartGetIceCandidate)
        {
            StartCoroutine(GetIceCandidateCorotine());
        }
    }

    public void SendSdpDataToServer(Message msg)
    {
        StartCoroutine(PostToServer(msg));
    }

    private IEnumerator PostToServer(Message msg)
    {
        var jsonData = JsonUtility.ToJson(msg);
        var byteData = System.Text.Encoding.UTF8.GetBytes(jsonData);
        DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
        string sendUrl = $"{HttpServerAddress}/setConnection/{LocalPeerId}";
        Debug.Log(jsonData);
        var www = new UnityWebRequest(sendUrl, UnityWebRequest.kHttpVerbPOST);
        www.downloadHandler = dH;
        www.uploadHandler = new UploadHandlerRaw(byteData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log($"failed to send message to peer {www.error}");
        }

        if (!www.isNetworkError || !www.isHttpError)
        {
            var json = www.downloadHandler.text;
            Debug.Log(json);
            if (json.Trim() != "")
            {
                Message mainmsg = JsonUtility.FromJson<Message>(json);
                if (mainmsg.Type == 1)
                {
                    if (peerUnity)
                    {
                        var base64EncodeBytes = Convert.FromBase64String(mainmsg.Data);
                        string Data = System.Text.Encoding.UTF8.GetString(base64EncodeBytes);
                        Debug.Log(Data);
                        peerUnity.SetRemoteDescription(Data);
                        StartGetIceCandidate = true;
                        lastComplite = true;
                    }
                }
            }
        }
    }

    IEnumerator GetIceCandidateCorotine()
    {
        if (HttpServerAddress.Length == 0)
        {
            throw new InvalidOperationException("error");
        }

        string sendUrl = $"{HttpServerAddress}/iceCandidate/{LocalPeerId}";
        var www = UnityWebRequest.Get(sendUrl);

        yield return www.SendWebRequest();

        if (!www.isNetworkError || !www.isHttpError)
        {
            var json = www.downloadHandler.text;
            if (json.Trim() != "")
            {
                Debug.Log(json);
                var msg = JsonUtility.FromJson<Message>(json);
                if (msg != null)
                {
                    var base64EncodeBytes = Convert.FromBase64String(msg.Data);
                    string Data = System.Text.Encoding.UTF8.GetString(base64EncodeBytes);

                    if (msg.Type == 2 && peerUnity)
                    {
                        var parts = Data.Split(new string[] { msg.IceDataSeparator }, StringSplitOptions.RemoveEmptyEntries);
                        peerUnity.OnIceCandidate(parts[0], parts[1], parts[2]);
                    }
                }
                else
                {
                    Debug.LogError($"Failed to deserialize JSON message : {json}");
                    gotError++;
                }
            }
        }
        else if (www.isNetworkError)
        {
            Debug.LogError($"Network error trying to send data to {HttpServerAddress}: {www.error}");
            gotError++;
        }
        lastComplite = true;
    }

    public void SendIceCandidate(string candidate, int sdpMlineIndex, string sdpMid)
    {
        StartCoroutine(PostIceCandidateServer(new Message
        {
            Data = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{candidate}|{sdpMlineIndex}|{sdpMid}")),
            IceDataSeparator = "|"
        }));
    }

    private IEnumerator PostIceCandidateServer(Message msg)
    {
        //WWWForm form = new WWWForm();
        //form.AddField("MSG", msg.msg);
        var jsonData = JsonUtility.ToJson(msg);
        //Debug.Log(jsonData);
        var bytedata = System.Text.Encoding.UTF8.GetBytes(jsonData);
        //yield return new WaitForSeconds(0.01f);
        DownloadHandlerBuffer dH = new DownloadHandlerBuffer();

        string sendUrl = $"{HttpServerAddress}/iceCandidate/{LocalPeerId}";
        Debug.Log(jsonData);
        //var www = UnityWebRequest.Post(sendUrl, jsonData);
        var www = new UnityWebRequest(sendUrl, UnityWebRequest.kHttpVerbPOST);
        www.downloadHandler = dH;
        www.uploadHandler = new UploadHandlerRaw(bytedata);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log($"Failed to send message to remote peer {LocalPeerId}: {www.error}");
        }

        if (!www.isNetworkError && !www.isHttpError)
        {
            //Debug.Log(www.downloadProgress);
            //Debug.Log(www.downloadHandler.text);
            var json = www.downloadHandler.text;
            if (json.Trim() != "")
            {
                Debug.Log(json);
            }
        }
    }
}

[Serializable]
public class Message
{
    public int Type;
    public int speakerId;
    public string ConnectionType;
    public string Data;
    public string IceDataCandidate;
    public string IceDataSeparator;
}